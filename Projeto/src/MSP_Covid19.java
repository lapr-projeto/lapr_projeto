import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class MSP_Covid19 {
    static File ficheiroInterativo = new File("Outputs/OutputInterativo.csv"); //Serve apenas para se conseguir a reutiliza��o de m�todos para a parte interativa e n�o-interativa
    static Scanner lerUser = new Scanner(System.in);

    public static void main(String[] args) throws IOException, ParseException {
        //Parte n�o-interativa da Aplica��o!
        if (args.length > 1) {
                executarParteNaoInterativa(args);
        } else {
            //Parte Interativa

            String[][] matrizCovidNovosCasos = new String[0][0], matrizCovidAcumulado = new String[0][0];
            double[][] matrizTransicao = new double[0][0];
            boolean fichNovosCasos = false, fichAcumulado = false, fichTransicao = false;

            String escolha;
            System.out.print("Bem-Vindo!\n");

            do {
                File ficheiroCovid = carregarFicheiro();
                int tipoFicheiroCarregado = tipoFicheiro(ficheiroCovid);
                if (tipoFicheiroCarregado == 0) {
                    matrizCovidAcumulado = criarMatrizCovid(ficheiroCovid);
                    fichAcumulado = true;
                } else if (tipoFicheiroCarregado == 1) {
                    matrizCovidNovosCasos = criarMatrizCovid(ficheiroCovid);
                    fichNovosCasos = true;
                }
                else if (tipoFicheiroCarregado == 2){
                    matrizTransicao = criarMatrizTransicaoMarkov(ficheiroCovid);
                    fichTransicao = true;
                }

                System.out.print("Deseja carregar mais algum ficheiro? (s/n)\n");
                escolha = lerUser.nextLine();
                while (!escolha.equals("S") && !escolha.equals("s") && !escolha.equals("n") && !escolha.equals("N")) {
                    System.out.println("Op��o Inv�lida!");
                    System.out.print("Deseja carregar mais algum ficheiro? (s/n)\n");
                    escolha = lerUser.nextLine();
                }
            }while (!escolha.equals("n") && !escolha.equals("N"));

            String opcao1;
            do {
                System.out.print("Escolha uma das op��es:\n1 - An�lise di�ria\n2 - An�lise num per�odo de tempo\n" +
                        "3 - An�lise comparativa\n4 - Previs�o de dados\n5 - Previs�o do n�mero esperado de dias at� � morte\n6 - Carregar ficheiro de dados\n7 - Sair\n");
                opcao1 = lerUser.next();
                switch (opcao1) {
                    case "1":
                        if(fichNovosCasos && !fichAcumulado){
                            analiseDiariaCovid(matrizCovidNovosCasos);
                        }
                        else if(!fichNovosCasos && fichAcumulado){
                            analiseDiariaCovid(matrizCovidAcumulado);
                        }
                        else if(fichAcumulado && fichNovosCasos){
                            System.out.print("Escolha uma das seguintes op��es\n1 - Analisar ficheiro de novos casos\n2 - Analisar ficheiro de dados acumulados\n");
                            String escolha2 = lerUser.next();
                            switch (escolha2){
                                case "1":
                                    analiseDiariaCovid(matrizCovidNovosCasos);
                                    break;
                                case "2":
                                    analiseDiariaCovid(matrizCovidAcumulado);
                                    break;
                                default:
                                    System.out.println("Op��o Inv�lida");
                                    break;
                            }
                        }
                        break;
                    case "2":
                        System.out.print("Insira a data inicial (aaaa-mm-dd):");
                        String dataInicial = converterFormatoData(lerUser.next());
                        System.out.print("Insira a data final (aaaa-mm-dd):");
                        String dataFinal = converterFormatoData(lerUser.next());

                        if(verificarDataValida(dataInicial) && verificarDataValida(dataFinal)){
                            if(verificarPeriodoValido(dataInicial,dataFinal)) {
                                System.out.println("Escolha uma das op��es:\n1 - An�lise di�ria\n2 - An�lise semanal\n3 - An�lise mensal");
                                String opcao2 = lerUser.next();
                                switch (opcao2) {
                                    case "1":
                                        if (fichNovosCasos && !fichAcumulado) {
                                            analiseDiariaCovidIntervaloTempo(matrizCovidNovosCasos, dataFinal, dataInicial, new FileOutputStream(ficheiroInterativo), true);
                                        } else if (!fichNovosCasos && fichAcumulado) {
                                            analiseDiariaCovidIntervaloTempo(matrizCovidAcumulado, dataFinal, dataInicial, new FileOutputStream(ficheiroInterativo), true);
                                        } else {
                                            System.out.print("Escolha uma das seguintes op��es\n1 - Analisar ficheiro de novos casos\n2 - Analisar ficheiro de dados acumulados\n");
                                            String escolha2 = lerUser.next();
                                            switch (escolha2) {
                                                case "1":
                                                    analiseDiariaCovidIntervaloTempo(matrizCovidNovosCasos, dataFinal, dataInicial, new FileOutputStream(ficheiroInterativo), true);
                                                    break;
                                                case "2":
                                                    analiseDiariaCovidIntervaloTempo(matrizCovidAcumulado, dataFinal, dataInicial, new FileOutputStream(ficheiroInterativo), true);
                                                    break;
                                                default:
                                                    System.out.println("Op��o Inv�lida");
                                                    break;
                                            }
                                        }
                                        break;
                                    case "2":
                                        if (fichNovosCasos && !fichAcumulado) {
                                            analiseSemanal(matrizCovidNovosCasos, dataInicial, dataFinal, new FileOutputStream(ficheiroInterativo), true);
                                        } else if (!fichNovosCasos && fichAcumulado) {
                                            analiseSemanal(matrizCovidAcumulado, dataInicial, dataFinal, new FileOutputStream(ficheiroInterativo), true);
                                        } else {
                                            System.out.print("Escolha uma das seguintes op��es\n1 - Analisar ficheiro de novos casos\n2 - Analisar ficheiro de dados acumulados\n");
                                            String escolha2 = lerUser.next();
                                            switch (escolha2) {
                                                case "1":
                                                    analiseSemanal(matrizCovidNovosCasos, dataInicial, dataFinal, new FileOutputStream(ficheiroInterativo), true);
                                                    break;
                                                case "2":
                                                    analiseSemanal(matrizCovidAcumulado, dataInicial, dataFinal, new FileOutputStream(ficheiroInterativo), true);
                                                    break;
                                                default:
                                                    System.out.println("Op��o Inv�lida");
                                                    break;
                                            }
                                        }
                                        break;
                                    case "3":
                                        if (fichNovosCasos && !fichAcumulado) {
                                            analiseMensal(matrizCovidNovosCasos, dataInicial, dataFinal, new FileOutputStream(ficheiroInterativo), true);
                                        } else if (!fichNovosCasos && fichAcumulado) {
                                            analiseMensal(matrizCovidAcumulado, dataInicial, dataFinal, new FileOutputStream(ficheiroInterativo), true);
                                        } else {
                                            System.out.print("Escolha uma das seguintes op��es\n1 - Analisar ficheiro de novos casos\n2 - Analisar ficheiro de dados acumulados\n");
                                            String escolha2 = lerUser.next();
                                            switch (escolha2) {
                                                case "1":
                                                    analiseMensal(matrizCovidNovosCasos, dataInicial, dataFinal, new FileOutputStream(ficheiroInterativo), true);
                                                    break;
                                                case "2":
                                                    analiseMensal(matrizCovidAcumulado, dataInicial, dataFinal, new FileOutputStream(ficheiroInterativo), true);
                                                    break;
                                                default:
                                                    System.out.println("Op��o Inv�lida");
                                                    break;
                                            }
                                        }
                                        break;
                                    default:
                                        System.out.println("Op��o Inv�lida");
                                        break;
                                }
                            }else System.out.println("Per�odo Inv�lido");
                        }else System.out.println("Datas Inv�lidas");
                        break;
                    case "3":
                        System.out.println("Introduza a data inicial do primeiro per�odo a analisar (aaaa-mm-dd):");
                        String dataInicial1 = converterFormatoData(lerUser.next());
                        System.out.println("Introduza a data final do primeiro per�odo a analisar (aaaa-mm-dd):");
                        String dataFinal1 = converterFormatoData(lerUser.next());

                        System.out.println("Introduza a data inicial do segundo per�odo a analisar (aaaa-mm-dd):");
                        String dataInicial2 = converterFormatoData(lerUser.next());
                        System.out.println("Introduza a data final do segundo per�odo a analisar (aaaa-mm-dd):");
                        String dataFinal2 = converterFormatoData(lerUser.next());

                        if (verificarDataValida(dataInicial1) && verificarDataValida(dataFinal1) && verificarDataValida(dataInicial2) && verificarDataValida(dataFinal2)) {
                            if(verificarPeriodoValido(dataInicial1,dataFinal1) && verificarPeriodoValido(dataInicial2,dataFinal2)) {
                                if (fichNovosCasos && !fichAcumulado) {
                                    analiseComparativa(matrizCovidNovosCasos, dataInicial1, dataFinal1, dataInicial2, dataFinal2, new FileOutputStream(ficheiroInterativo), true);
                                } else if (!fichNovosCasos && fichAcumulado) {
                                    analiseComparativa(matrizCovidAcumulado, dataInicial1, dataFinal1, dataInicial2, dataFinal2, new FileOutputStream(ficheiroInterativo), true);
                                } else {
                                    System.out.print("Escolha uma das seguintes op��es\n1 - Analisar ficheiro de novos casos\n2 - Analisar ficheiro de dados acumulados\n");
                                    String escolha2 = lerUser.next();
                                    switch (escolha2) {
                                        case "1":
                                            analiseComparativa(matrizCovidNovosCasos, dataInicial1, dataFinal1, dataInicial2, dataFinal2, new FileOutputStream(ficheiroInterativo), true);
                                            break;
                                        case "2":
                                            analiseComparativa(matrizCovidAcumulado, dataInicial1, dataFinal1, dataInicial2, dataFinal2, new FileOutputStream(ficheiroInterativo), true);
                                            break;
                                        default:
                                            System.out.println("Op��o Inv�lida");
                                            break;
                                    }
                                }
                            } else System.out.println("Per�odo Inv�lido");
                        }else System.out.println("Datas Inv�lidas");

                        break;
                    case "4":
                        if(fichTransicao && fichNovosCasos){
                            System.out.println("Introduza a data para a previs�o (aaaa-mm-dd): ");
                            String data = converterFormatoData(lerUser.next());
                            String dataerro = converterFormatoData(matrizCovidNovosCasos[1][0]);
                            if (data.equals(dataerro) || converterFormatoData(data).equals("Data Inv�lida")|| !verificarDataAntiga(data,matrizCovidNovosCasos[1][0])) {
                                System.out.println("Data Inv�lida!");
                            } else {
                               double [] arrayPrevisao = previsaoMain(matrizTransicao, data, matrizCovidNovosCasos);
                               String[][] matrizPrevisao = criarMatrizPrevisao(arrayPrevisao,data);
                               imprimirMatrizPrevisao(matrizPrevisao,new FileOutputStream(ficheiroInterativo),true);
                            }
                        }
                        else if(!fichTransicao && fichNovosCasos) {
                            System.out.println("Ficheiro com a matriz de transi��es n�o carregado!");
                        }
                        else if(fichTransicao && !fichNovosCasos){
                            System.out.println("Ficheiro de novos casos n�o carregado!");
                        }
                        else{
                            System.out.println("Ficheiro com a matriz de transi��es e ficheiro de novos casos n�o carregados!");
                        }

                        break;
                    case "5":
                        if(fichTransicao){
                            double[][] matrizM = matrizM(matrizTransicao);
                            String[][] matrizPrevisaoDias = criarMatrizPrevisaoDiasAteObito(matrizM);
                            imprimirMatrizPrevisaoDiasAteObito(matrizPrevisaoDias,new FileOutputStream(ficheiroInterativo),true);
                        }
                        else{
                            System.out.println("Ficheiro com a matriz de transi��es n�o carregado!");
                        }
                        break;
                    case "6":
                        do {
                            lerUser.nextLine();
                            File ficheiroCovid = carregarFicheiro();
                            int tipoFicheiroCarregado = tipoFicheiro(ficheiroCovid);
                            if (tipoFicheiroCarregado == 0) {
                                matrizCovidAcumulado = criarMatrizCovid(ficheiroCovid);
                                fichAcumulado = true;
                            } else if (tipoFicheiroCarregado == 1) {
                                matrizCovidNovosCasos = criarMatrizCovid(ficheiroCovid);
                                fichNovosCasos = true;
                            }
                            else if (tipoFicheiroCarregado == 2){
                                matrizTransicao=criarMatrizTransicaoMarkov(ficheiroCovid);
                                fichTransicao = true;
                            }

                            System.out.print("Deseja carregar mais algum ficheiro? (s/n)\n");
                            escolha = lerUser.next();
                            while (!escolha.equals("S") && !escolha.equals("s") && !escolha.equals("n") && !escolha.equals("N")) {
                                System.out.println("Op��o Inv�lida!");
                                System.out.print("Deseja carregar mais algum ficheiro? (s/n)\n");
                                escolha = lerUser.next();
                            }

                        }while (!escolha.equals("n") && !escolha.equals("N"));
                        break;
                    case "7":
                        System.out.println("At� � pr�xima visita!");
                        break;
                    default:
                        System.out.println("Op��o Inv�lida");
                        break;
                }
            } while (!opcao1.equals("7"));
        }
    }

    public static void executarParteNaoInterativa(String[] args) throws IOException, ParseException {
        String paramInputNumerosTotais = "", paramInputNumerosAcumulados = "", paramInputMatrizTransicao= "", paramFicheiroOutput= "";

        String paramTipoAnalise = "", paramDataInicial = "", paramDataFinal = "", paramDataPrevisao = "";
        String paramDataInicial1 = "", paramDataInicial2 = "", paramDataFinal1 = "", paramDataFinal2 = "";
        boolean transicao = false, periodico = false;

        for (int parametro = 0; parametro < args.length; parametro++) {
            if (args[parametro].equals("-r")) {
                paramTipoAnalise = args[parametro + 1];
                periodico = true;
            } else if (args[parametro].equals("-di")) {
                paramDataInicial = converterFormatoData(args[parametro + 1]);
            } else if (args[parametro].equals("-df")) {
                paramDataFinal = converterFormatoData(args[parametro + 1]);
            } else if (args[parametro].equals("-di1")) {
                paramDataInicial1 = converterFormatoData(args[parametro + 1]);
                periodico = true;
            } else if (args[parametro].equals("-di2")) {
                paramDataInicial2 = converterFormatoData(args[parametro + 1]);
            } else if (args[parametro].equals("-df1")) {
                paramDataFinal1 = converterFormatoData(args[parametro + 1]);
            } else if (args[parametro].equals("-df2")) {
                paramDataFinal2 = converterFormatoData(args[parametro + 1]);
            } else if (args[parametro].equals("-T")) {
                paramDataPrevisao = args[parametro + 1];
                transicao = true;
            }
        }
        paramFicheiroOutput = args[args.length - 1];

        if (transicao && !periodico){
            paramInputNumerosTotais = args[args.length - 3];
            paramInputMatrizTransicao = args[args.length - 2];
        }
        if (!transicao && periodico){
            paramInputNumerosAcumulados = args[args.length - 2];
        }else if(transicao && periodico){
            paramInputNumerosTotais = args[args.length - 4];
            paramInputNumerosAcumulados = args[args.length - 3];
            paramInputMatrizTransicao = args[args.length - 2];
        }


        File inputNumerosTotais = new File("Inputs", paramInputNumerosTotais);
        File inputNumerosAcumulados = new File("Inputs", paramInputNumerosAcumulados);
        File inputMatrizTransicao = new File("Inputs", paramInputMatrizTransicao);
        File fileOutputParametro = new File("Outputs", paramFicheiroOutput);
        FileOutputStream outputParametro = new FileOutputStream(fileOutputParametro,false);


        String[][] matrizCovidNaoInterativaCasosTotais = new String[0][0], matrizCovidNaoInterativaAcumulados = new String[0][0];
        if (transicao && !periodico){
            matrizCovidNaoInterativaCasosTotais = criarMatrizCovid(inputNumerosTotais);
        }
        if (!transicao && periodico){
            matrizCovidNaoInterativaAcumulados = criarMatrizCovid(inputNumerosAcumulados);
        }else if(transicao && periodico){
            matrizCovidNaoInterativaAcumulados = criarMatrizCovid(inputNumerosAcumulados);
            matrizCovidNaoInterativaCasosTotais = criarMatrizCovid(inputNumerosTotais);
        }


        PrintStream printFicheiroExterno = new PrintStream(outputParametro);
        if(!paramTipoAnalise.equals("")) {
            if (verificarPeriodoValido(paramDataInicial, paramDataFinal)) {
                switch (paramTipoAnalise) {
                    case "0":
                        printFicheiroExterno.println("An�lise Di�ria de " + paramDataInicial + " at� " + paramDataFinal);
                        analiseDiariaCovidIntervaloTempo(matrizCovidNaoInterativaAcumulados, paramDataFinal, paramDataInicial, outputParametro, false);
                        printFicheiroExterno.println();
                        System.out.println("Resultados da An�lise Di�ria guardados em " + fileOutputParametro.getAbsolutePath());
                        break;
                    case "1":
                        printFicheiroExterno.println("An�lise Semanal de " + paramDataInicial + " at� " + paramDataFinal);
                        analiseSemanal(matrizCovidNaoInterativaAcumulados, paramDataInicial, paramDataFinal, outputParametro, false);
                        printFicheiroExterno.println();
                        System.out.println("Resultados da An�lise Semanal guardados em " + fileOutputParametro.getAbsolutePath());
                        break;
                    case "2":
                        printFicheiroExterno.println("An�lise Mensal de " + paramDataInicial + " at� " + paramDataFinal);
                        analiseMensal(matrizCovidNaoInterativaAcumulados, paramDataInicial, paramDataFinal, outputParametro, false);
                        printFicheiroExterno.println();
                        System.out.println("Resultados da An�lise Mensal guardados em " + fileOutputParametro.getAbsolutePath());
                        break;
                }
            } else {
                printFicheiroExterno.println("Per�odo Inv�lido");
            }
        }

        if (!paramDataInicial1.equals("") && !paramDataInicial2.equals("") && !paramDataFinal1.equals("") && !paramDataFinal2.equals("")) {
            if(verificarPeriodoValido(paramDataInicial1,paramDataFinal1) && verificarPeriodoValido(paramDataInicial2,paramDataFinal2)){
                printFicheiroExterno.println("An�lise Comparativa entre os per�odos de " + paramDataInicial1 + " at� "+ paramDataFinal1 + " e " + paramDataInicial2 + " at� "+ paramDataFinal2);
                analiseComparativa(matrizCovidNaoInterativaAcumulados, paramDataInicial1, paramDataFinal1, paramDataInicial2, paramDataFinal2, outputParametro, false);
                printFicheiroExterno.println();
                System.out.println("Resultados da An�lise Comparativa guardados em " + fileOutputParametro.getAbsolutePath());
            }
            else {
                printFicheiroExterno.println("Per�odo Inv�lido");
            }
        }

        if (!paramDataPrevisao.equals("")){
            double[][] matrizTransicaoMarkov = criarMatrizTransicaoMarkov(inputMatrizTransicao);
            double [] arrayPrevisao = previsaoMain(matrizTransicaoMarkov, paramDataPrevisao, matrizCovidNaoInterativaCasosTotais);
            String[][] matrizPrevisao = criarMatrizPrevisao(arrayPrevisao,paramDataPrevisao);
            imprimirMatrizPrevisao(matrizPrevisao,outputParametro,false);
            double[][] matrizM = matrizM(matrizTransicaoMarkov);
            String[][] matrizPrevisaoDias = criarMatrizPrevisaoDiasAteObito(matrizM);
            imprimirMatrizPrevisaoDiasAteObito(matrizPrevisaoDias,outputParametro,false);
            System.out.println("Resultados das Previs�es guardados em " + fileOutputParametro.getAbsolutePath());
        }

        outputParametro.close();

    }

    // M�todo que conta e retorna o n�mero de linhas do ficheiro

    public static int countLinhasMatrizCovid(File input) throws FileNotFoundException {
        Scanner ler = new Scanner(input);
        int countlinhas = 0;
        while (ler.hasNextLine()) {
            ler.nextLine();
            countlinhas++;
        }
        ler.close();
        return countlinhas;
    }

    // M�todo que conta e retorna o n�mero de colunas do ficheiro

    public static int countColunasMatrizCovid(File input) throws FileNotFoundException {
        Scanner ler = new Scanner(input);
        String[] elements = ler.nextLine().split(",");
        ler.close();

        return elements.length;
    }

    // M�todo que cria uma matriz com o n�mero de linhas e colunas do ficheiro e que copia os seus dados para essa mesma matriz

    public static String[][] criarMatrizCovid(File input) throws FileNotFoundException {
        Scanner ler = new Scanner(input);
        String[][] matrizCovid = new String[countLinhasMatrizCovid(input)][countColunasMatrizCovid(input)];

        for (int linha = 0; linha < matrizCovid.length; linha++) {
            String[] elements = ler.nextLine().split(",");
            for (int coluna = 0; coluna < matrizCovid[0].length; coluna++) {
                matrizCovid[linha][coluna] = elements[coluna];
            }
        }
        ler.close();
        return matrizCovid;
    }

    // M�todo que verifica se o tipo de matriz utilizada � dos dados acumulados

    public static boolean verificarSeDadosAcumulados(String[][] arr) {
        for (int coluna = 0; coluna < arr[0].length; coluna++) {
            if (arr[0][coluna].contains("acumulado")) {
                return true;
            }
        }
        return false;
    }

    // M�todo para verificar se o n�mero introduzido � positivo, negativo ou zero, e imprime o respetivo sinal antes do n�mero

    public static String verificarSinal(int num) {
        if (num > 0) {
            return "+" + num;
        } else if (num == 0) {
            return "0";
        } else {
            return "-" + Math.abs(num);
        }

    }

    // M�todo que verifica se a data introduzida � v�lida e a converte no formato AAAA-MM-DD, caso seja introduzida no formato DD-MM-AAAA.
    // Tamb�m adiciona um "0" antes do dia ou do m�s caso um destes tenha sido introduzido com apenas um d�gito

    public static String converterFormatoData(String data) {
        String aux;
        String[] arrayData = data.split("-");

        if (arrayData.length < 3) {
            return "Data Inv�lida";
        } else {
            String ano = arrayData[0];
            String mes = arrayData[1];
            String dia = arrayData[2];

            if (Integer.parseInt(dia) > 31) {
                aux = ano;
                ano = dia;
                dia = aux;
            }

            if (dia.charAt(0) != '0' && dia.length() == 1) {
                dia = "0" + dia;
            }

            if (mes.charAt(0) != '0' && mes.length() == 1) {
                mes = "0" + mes;
            }
            return ano + "-" + mes + "-" + dia;
        }
    }

    //M�todo que verifica se a data introduzida � v�lida

    public static boolean verificarDataValida(String data) {
        String[] arrayData = data.split("-");
        if (arrayData.length < 3) {
            return false;
        }
        int ano = Integer.parseInt(arrayData[0]);
        int mes = Integer.parseInt(arrayData[1]);
        int dia = Integer.parseInt(arrayData[2]);

        if (ano <= 2022 && mes >= 1 && mes <= 12 && dia >= 1 && dia <= 31) {
            if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia <= 30) {
                return true;
            } else if ((mes == 2 && dia <= 29 && verificarAnoBissexto(ano)) || (mes == 2 && dia <= 28 && !verificarAnoBissexto(ano))) {
                return true;
            } else if ((mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)) {
                return true;
            }
            return false;
        }
        return false;
    }

    // M�todo para verificar se o ano da data introduzida � um ano bissexto

    public static boolean verificarAnoBissexto(int ano) {
        if (ano % 400 == 0 || (ano % 4 == 0 && ano % 100 != 0)) {
            return true;
        }
        return false;
    }

    // M�todo para verificar em que linha da matriz se encontra a data introduzida

    public static int verificarLinhaDataFicheiro(String[][] arr, String data) {
        for (int linha = 1; linha < arr.length; linha++) {
            String dataFicheiro = converterFormatoData(arr[linha][0]);
            if (dataFicheiro.equals(data)) {
                return linha;
            }
        }
        return -1;
    }

    // M�todo que pergunta ao utilizador se pretende guardar o resultado apresentado num ficheiro e caso a resposta seja sim, pergunta tamb�m o nome a dar ao ficheiro.

    public static String guardarResultadoEmFicheiro() {
        System.out.println("Guardar resultados num ficheiro CSV? (s/n)");
        String opcao;
        opcao = lerUser.next();
        while (!opcao.equals("n") && !opcao.equals("N") && !opcao.equals("S") && !opcao.equals("s")) {
            System.out.println("Op��o Inv�lida");
            System.out.println("Guardar resultados num ficheiro CSV? (s/n)");
            opcao = lerUser.next();
        }
        lerUser.nextLine();
        if (opcao.equals("s") || opcao.equals("S")) {
            System.out.println("Indique o nome do ficheiro:");
            return lerUser.nextLine() + ".csv";
        } else {
            return "n";
        }
    }

    // M�todo para criar uma matriz em que s�o apresentados os dados dos per�odos de tempo definidos

    public static String[][] criarMatrizDados(String[][] arr, String data, int linhaInicial, int linhaFinal, boolean novosCasos) {
        String[][] matrizCasos = new String[5][3];

        matrizCasos[0][1] = data;
        matrizCasos[2][0] = "Internamentos";
        matrizCasos[3][0] = "Internamentos em UCI";
        matrizCasos[4][0] = "�bitos";

        matrizCasos[1][1] = arr[linhaFinal][2];
        matrizCasos[2][1] = arr[linhaFinal][3];
        matrizCasos[3][1] = arr[linhaFinal][4];


        if(verificarSeDadosAcumulados(arr)){
            matrizCasos[1][0] = "Infetados";
            matrizCasos[4][1] = arr[linhaFinal][5];
            matrizCasos[0][0] = "Dados acumulados";
        }
        else{
            matrizCasos[1][0] = "Casos Ativos";
            matrizCasos[4][2] = verificarSinal(Integer.parseInt(arr[linhaFinal][5]));
            matrizCasos[0][0] = "Total de Casos";
        }

        if (linhaFinal > 1 && novosCasos) {
            int varCasos = Integer.parseInt(arr[linhaFinal][2]) - Integer.parseInt(arr[linhaInicial][2]);
            int varInt = Integer.parseInt(arr[linhaFinal][3]) - Integer.parseInt(arr[linhaInicial][3]);
            int varUCI = Integer.parseInt(arr[linhaFinal][4]) - Integer.parseInt(arr[linhaInicial][4]);

            matrizCasos[1][2] = verificarSinal(varCasos);
            matrizCasos[2][2] = verificarSinal(varInt);
            matrizCasos[3][2] = verificarSinal(varUCI);


            if(verificarSeDadosAcumulados(arr)){
                matrizCasos[4][2] = verificarSinal(Integer.parseInt(arr[linhaFinal][5]) - Integer.parseInt(arr[linhaInicial][5]));
            }
            else{
                matrizCasos[4][1] = "";
            }

        } else if (novosCasos) {
            matrizCasos[2][2] = "  N�o h� registos";
            matrizCasos[3][2] = "  do dia anterior";
        }

        return matrizCasos;
    }

    // M�todo para carregar um novo ficheiro para o programa

    public static File carregarFicheiro(){
        System.out.println("Insira o caminho absoluto do ficheiro que pretende carregar: ");
        String caminho = lerUser.nextLine();
        File ficheiro = new File(caminho);
        while (!ficheiro.exists()){
            System.out.println("Ficheiro n�o encontrado!");
            System.out.println("Insira o caminho absoluto do ficheiro que pretende carregar: ");
            caminho = lerUser.nextLine();
            ficheiro = new File(caminho);
        }
        return ficheiro;
    }

    // M�todo que verifica e retorna o tipo de ficheiro carregado para al�m de dizer ao utilizador qual o tipo de ficheiro que carregou

    public static int tipoFicheiro(File ficheiro) throws FileNotFoundException {

        Scanner lerFicheiro = new Scanner(ficheiro);
        while (lerFicheiro.hasNext()){
            String testFicheiro = lerFicheiro.next().toLowerCase();
            if (testFicheiro.contains("acumulado")){
                System.out.println("Ficheiro de dados acumulados carregado com sucesso!");
                return 0;
            }
            else if(testFicheiro.contains("infetado")){
                System.out.println("Ficheiro de novos casos carregado com sucesso!");
                return 1;
            }
        }
        System.out.println("Ficheiro de matriz de transi��es carregado com sucesso!");
        return 2;
    }

    // M�todo que realiza a an�lise dos dados no dia introduzido pelo utilizador, verificando se a data � v�lida e existente no ficheiro, retornando uma matriz com os resultados
    // Este m�todo permite tamb�m ao utilizador escolher se pretende guardar os resultados apresentados

    public static void analiseDiariaCovid(String[][] arr) throws FileNotFoundException {

        System.out.println("Insira a data a analisar (aaaa-mm-dd):");
        String data = converterFormatoData(lerUser.next());
        if (verificarDataValida(data)) {
            int linha = verificarLinhaDataFicheiro(arr, data);
            if (linha != -1) {
                String[][] matrizDiaria = criarMatrizDados(arr, data, linha - 1, linha, true);
                matrizDiaria[0][2] = "     Em rela��o ao dia anterior";
                imprimirMatriz(matrizDiaria, System.out);
                String guardarFicheiro = guardarResultadoEmFicheiro();
                if (!guardarFicheiro.equals("n")) {
                    File ficheiroOutput = new File("Outputs", guardarFicheiro);
                    PrintStream outTXT = new PrintStream(ficheiroOutput);
                    matrizDiaria[0][2] = "     Em rela��o ao dia anterior";
                    imprimirMatriz(matrizDiaria, outTXT);
                    outTXT.close();
                    System.out.println("Resultados guardados em " + ficheiroOutput.getAbsolutePath());
                }
            } else {
                System.out.println("Data escolhida n�o est� presente no ficheiro.");
            }
        } else {
            System.out.println("Data inv�lida");
        }

    }

    // M�todo que realiza a an�lise dos dados di�rios num per�odo de tempo introduzido pelo utilizador, que caso as datas introduzidas existam no ficheiro retorna uma matriz com os dados para cada dia do periodo introduzido
    // Este m�todo permite tamb�m ao utilizador escolher se pretende guardar os resultados apresentados

    public static void analiseDiariaCovidIntervaloTempo(String[][] arr, String dataFinal, String dataInicial, FileOutputStream outputFile, boolean interativo) throws FileNotFoundException {
        PrintStream output = new PrintStream(outputFile);
        int linhaInicial = 0;
        int linhaFinal = 0;
        boolean dataInFicheiro = true;
        String [][] matrizDiariaIntervaloTempo;
        if (interativo) {
            output = new PrintStream(System.out);
        }

            linhaInicial = verificarLinhaDataFicheiro(arr, dataInicial);
            linhaFinal = verificarLinhaDataFicheiro(arr, dataFinal);

            if (linhaFinal == -1 || linhaInicial == -1) {
                output.println("A(s) data(s) escolhida(s) n�o existe(m) no ficheiro");
                dataInFicheiro = false;
            }
            if (dataInFicheiro) {
                    if (interativo) {
                        for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
                            matrizDiariaIntervaloTempo = criarMatrizDados(arr, arr[linha][0], linha - 1, linha, true);
                            matrizDiariaIntervaloTempo[0][2] = "     Em rela��o ao dia anterior";
                            imprimirMatriz(matrizDiariaIntervaloTempo, System.out);
                        }
                        String guardarFicheiro = guardarResultadoEmFicheiro();
                        if (!guardarFicheiro.equals("n")) {
                            File ficheiroOutput = new File("Outputs", guardarFicheiro);
                            PrintStream outTXT = new PrintStream(ficheiroOutput);
                            for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
                                matrizDiariaIntervaloTempo = criarMatrizDados(arr, arr[linha][0], linha - 1, linha, true);
                                matrizDiariaIntervaloTempo[0][2] = "     Em rela��o ao dia anterior";
                                imprimirMatriz(matrizDiariaIntervaloTempo, outTXT);
                            }
                            System.out.println("Resultados guardados em " + ficheiroOutput.getAbsolutePath());
                            outTXT.close();
                        }
                    } else {
                        for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
                            matrizDiariaIntervaloTempo = criarMatrizDados(arr, arr[linha][0], linha - 1, linha, true);
                            matrizDiariaIntervaloTempo[0][2] = "     Em rela��o ao dia anterior";
                            imprimirMatriz(matrizDiariaIntervaloTempo, output);
                        }
                    }
            }
    }

    // M�todo que realiza a an�lise dos dados semanais num per�odo de tempo introduzido pelo utilizador, que caso as datas introduzidas existam no ficheiro e completem pelo menos uma semana, retorna uma matriz com os dados para cada semana do periodo introduzido
    // Este m�todo permite tamb�m ao utilizador escolher se pretende guardar os resultados apresentados

    public static void analiseSemanal (String[][] arr,String dataInicial, String dataFinal,  FileOutputStream outputFile, boolean interativo) throws FileNotFoundException, ParseException {
        PrintStream output = new PrintStream(outputFile);

        if (interativo) {
            output = new PrintStream(System.out);
        }
        String [][] matrizSemanal;
        int linhaInicial = 0, linhaFinal = 0, linhaInicioSemanaSeguinte = 0,linhaInicioSemanaSeguinteOutput = 0;
        boolean dataInFicheiro = true;


            linhaInicial = verificarLinhaDataFicheiro(arr, dataInicial);
            linhaFinal = verificarLinhaDataFicheiro(arr, dataFinal);
            linhaInicioSemanaSeguinte = verificarLinhaInicioSemana(arr,dataInicial);
            linhaInicioSemanaSeguinteOutput = linhaInicioSemanaSeguinte;

            if (linhaFinal == -1 || linhaInicial == -1) {
                output.println("A(s) data(s) escolhida(s) n�o existe(m) no ficheiro");
                dataInFicheiro = false;
            }
            if (dataInFicheiro) {
                if (linhaInicioSemanaSeguinte + 6 > linhaFinal) {
                    output.println("O per�odo introduzido n�o cont�m uma semana completa");
                } else {
                    while (linhaInicioSemanaSeguinte + 6 < linhaFinal && linhaInicioSemanaSeguinte + 6 > 0) {
                        matrizSemanal = criarMatrizDados(arr, "                      De " + arr[linhaInicioSemanaSeguinte][0] + " at� " + arr[linhaInicioSemanaSeguinte + 6][0], linhaInicioSemanaSeguinte, linhaInicioSemanaSeguinte + 6, true);
                        if (!verificarSeDadosAcumulados(arr)){
                            matrizSemanal [4][1] ="";
                            matrizSemanal [4][2] = verificarSinal(somarObitosPeriodoTempo(arr,arr[linhaInicioSemanaSeguinte][0],arr[linhaInicioSemanaSeguinte + 6][0]));
                        }
                        imprimirMatriz(matrizSemanal, output);
                        linhaInicioSemanaSeguinte += 7;
                    }
                    if (interativo) {
                        String guardarFicheiro = guardarResultadoEmFicheiro();
                        if (!guardarFicheiro.equals("n")) {
                            File ficheiroOutput = new File("Outputs", guardarFicheiro);
                            PrintStream outTXT = new PrintStream(ficheiroOutput);
                            while (linhaInicioSemanaSeguinteOutput + 6 < linhaFinal && linhaInicioSemanaSeguinteOutput + 6 > 0) {
                                matrizSemanal = criarMatrizDados(arr, "                      De " + arr[linhaInicioSemanaSeguinteOutput][0] + " at� " + arr[linhaInicioSemanaSeguinteOutput + 6][0], linhaInicioSemanaSeguinteOutput, linhaInicioSemanaSeguinteOutput + 6, true);
                                if (!verificarSeDadosAcumulados(arr)){
                                    matrizSemanal [4][1] ="";
                                    matrizSemanal [4][2] = verificarSinal(somarObitosPeriodoTempo(arr,arr[linhaInicioSemanaSeguinteOutput][0],arr[linhaInicioSemanaSeguinteOutput + 6][0]));
                                }
                                imprimirMatriz(matrizSemanal, outTXT);
                                linhaInicioSemanaSeguinteOutput += 7;
                            }

                            System.out.println("Resultados guardados em " + ficheiroOutput.getAbsolutePath());
                            outTXT.close();
                        }
                    }
                }
            }
        }

    // M�todo que realiza a an�lise dos dados mensais num per�odo de tempo introduzido pelo utilizador, que caso as datas introduzidas existam no ficheiro e completem pelo menos um m�s, retorna uma matriz com os dados para cada m�s do periodo introduzido
    // Este m�todo permite tamb�m ao utilizador escolher se pretende guardar os resultados apresentados

    public static void analiseMensal(String[][] arr, String dataInicial, String dataFinal, FileOutputStream outputFile, boolean interativo) throws FileNotFoundException {
        PrintStream output = new PrintStream(outputFile);

        if (interativo) {
            output = new PrintStream(System.out);
        }

        int linhaInicial = 0, linhaFinal = 0, linhaInicioMesSeguinte = 0, linhaFimMesSeguinteOutput = 0, linhaInicioMesSeguinteOutput = 0, linhaFimMesSeguinte = 0;
        boolean dataInFicheiro = true;

            linhaInicial = verificarLinhaDataFicheiro(arr, dataInicial);
            linhaFinal = verificarLinhaDataFicheiro(arr, dataFinal);

            if(linhaInicial != -1){
                linhaInicioMesSeguinte = verificarLinhaInicioMes(arr, dataInicial);
                linhaFimMesSeguinte = verificarLinhaFimMes(arr, converterFormatoData(arr[linhaInicioMesSeguinte][0]));
                linhaFimMesSeguinteOutput = linhaFimMesSeguinte;
                linhaInicioMesSeguinteOutput = linhaInicioMesSeguinte;
            }

            if (linhaFinal == -1 || linhaInicial == -1) {
                output.println("A(s) data(s) escolhida(s) n�o existe(m) no ficheiro");
                dataInFicheiro = false;
            }
            String [][] matrizMensal;
            if (dataInFicheiro) {
                if (linhaInicioMesSeguinte > linhaFinal) {
                    output.println("O per�odo introduzido � inferior a 1 m�s.");
                }
                else {
                    while (linhaFimMesSeguinte <= linhaFinal && linhaInicioMesSeguinte > 0 && linhaFimMesSeguinte > 0){
                        matrizMensal = criarMatrizDados(arr, "                      De " + arr[linhaInicioMesSeguinte][0] + " at� " + arr[linhaFimMesSeguinte][0], linhaInicioMesSeguinte, linhaFimMesSeguinte, true);
                        if (!verificarSeDadosAcumulados(arr)){
                            matrizMensal [4][1] ="";
                            matrizMensal [4][2] = verificarSinal(somarObitosPeriodoTempo(arr,arr[linhaInicioMesSeguinte][0],arr[linhaFimMesSeguinte][0]));
                        }
                        imprimirMatriz(matrizMensal, output);
                        linhaInicioMesSeguinte = verificarLinhaInicioMes(arr, converterFormatoData(arr[linhaFimMesSeguinte][0]));
                        if(linhaInicioMesSeguinte != -1){
                            linhaFimMesSeguinte = verificarLinhaFimMes(arr, converterFormatoData(arr[linhaInicioMesSeguinte][0]));
                        }
                    }

                    if (interativo) {
                        String guardarFicheiro = guardarResultadoEmFicheiro();
                        if (!guardarFicheiro.equals("n")) {
                            File ficheiroOutput = new File("Outputs", guardarFicheiro);
                            PrintStream outTXT = new PrintStream(ficheiroOutput);

                            while (linhaFimMesSeguinteOutput <= linhaFinal && linhaInicioMesSeguinteOutput > 0 && linhaFimMesSeguinteOutput > 0) {
                                matrizMensal =criarMatrizDados(arr, "                      De " + arr[linhaInicioMesSeguinteOutput][0] + " at� " + arr[linhaFimMesSeguinteOutput][0], linhaInicioMesSeguinteOutput, linhaFimMesSeguinteOutput, true);
                                if (!verificarSeDadosAcumulados(arr)){
                                    matrizMensal [4][1] ="";
                                    matrizMensal [4][2] = verificarSinal(somarObitosPeriodoTempo(arr,arr[linhaInicioMesSeguinteOutput][0],arr[linhaFimMesSeguinteOutput][0]));
                                }
                                imprimirMatriz(matrizMensal, outTXT);
                                linhaInicioMesSeguinteOutput = verificarLinhaInicioMes(arr, converterFormatoData(arr[linhaFimMesSeguinteOutput][0]));
                                if(linhaInicioMesSeguinteOutput != -1){
                                    linhaFimMesSeguinteOutput = verificarLinhaFimMes(arr, converterFormatoData(arr[linhaInicioMesSeguinteOutput][0]));
                                }
                            }
                            System.out.println("Resultados guardados em " + ficheiroOutput.getAbsolutePath());
                            outTXT.close();
                        }
                    }
                }
            }
        }

        // M�todo que cria uma matriz que permite ao utilizador analisar os dados dos dias presentes em dois per�odos de tempo distintos e comparar os dados desses dois per�odos
        // Este m�todo permite tamb�m ao utilizador escolher se pretende guardar os resultados apresentados

    public static void analiseComparativa(String[][] arr, String dataInicial1, String dataFinal1, String dataInicial2, String dataFinal2, FileOutputStream outputFile, boolean interativo) throws FileNotFoundException {
        PrintStream output = new PrintStream(outputFile);

        if (interativo) {
            output = new PrintStream(System.out);
        }

        int linhaInicial, linhaFinal, linhaInicial2, linhaFinal2;
        boolean dataInFicheiro = true;
        String guardarFicheiro;


            linhaInicial = verificarLinhaDataFicheiro(arr, dataInicial1);
            linhaFinal = verificarLinhaDataFicheiro(arr, dataFinal1);
            linhaInicial2 = verificarLinhaDataFicheiro(arr, dataInicial2);
            linhaFinal2 = verificarLinhaDataFicheiro(arr, dataFinal2);
            if (linhaFinal == -1 || linhaInicial == -1 || linhaInicial2 == -1 || linhaFinal2 == -1) {
                output.println("A(s) data(s) escolhida(s) n�o existe(m) no ficheiro");
                dataInFicheiro = false;
            }
            if (dataInFicheiro) {
                    int contadorNrDias = 0;

                    int somaInfetados1 = 0, somaInfetados2 = 0, somaInternados1 = 0, somaInternados2 = 0;
                    int somaUCI1 = 0, somaUCI2 = 0, somaObitos1 = 0, somaObitos2 = 0;

                    double distMediaInfetados1 = 0, distMediaInfetados2 = 0, distMediaInternados1 = 0, distMediaInternados2 = 0;
                    double distMediaUCI1 = 0, distMediaUCI2 = 0, distMediaObitos1 = 0, distMediaObitos2 = 0;

                    int linhaInicialCopia = linhaInicial;
                    int linhaInicial2Copia = linhaInicial2;

                    while (linhaInicial <= linhaFinal && linhaInicial2 <= linhaFinal2) {
                            String[][] matriz3 = criarMatrizDados(arr, arr[linhaInicial][0], linhaInicial - 1, linhaInicial, true);
                            somaInfetados1 += Integer.parseInt(matriz3[1][2]);
                            somaInternados1 += Integer.parseInt(matriz3[2][2]);
                            somaUCI1 += Integer.parseInt(matriz3[3][2]);
                            somaObitos1 += Integer.parseInt(matriz3[4][2]);
                            linhaInicial++;

                            String[][] matriz4 = criarMatrizDados(arr, arr[linhaInicial2][0], linhaInicial2 - 1, linhaInicial2, true);
                            somaInfetados2 += Integer.parseInt(matriz4[1][2]);
                            somaInternados2 += Integer.parseInt(matriz4[2][2]);
                            somaUCI2 += Integer.parseInt(matriz4[3][2]);
                            somaObitos2 += Integer.parseInt(matriz4[4][2]);
                            linhaInicial2++;

                        String[][] matrizComparativa = criarMatrizComparativa(matriz3, matriz4);
                        if(verificarSeDadosAcumulados(arr)){
                            matrizComparativa[0][2] = matrizComparativa[0][1];
                            matrizComparativa[0][1] = null;

                            matrizComparativa[0][4] = matrizComparativa[0][3];
                            matrizComparativa[0][3] = null;
                        }
                        else{
                            matrizComparativa[0][2] = "Varia��o";
                            matrizComparativa[0][4] = matrizComparativa[0][2];
                        }


                        if (interativo) {
                            imprimirMatriz(matrizComparativa, System.out);
                        } else {
                            imprimirMatriz(matrizComparativa, output);
                        }

                    contadorNrDias++;
                    }

                    for (int linha = linhaInicialCopia; linha < linhaInicialCopia + contadorNrDias; linha++) {
                        String[][] matriz5 = criarMatrizDados(arr, arr[linha][0], linha - 1, linha, true);
                        distMediaInfetados1 += Math.pow(Double.parseDouble(matriz5[1][2]) - ((double) somaInfetados1 / contadorNrDias), 2);
                        distMediaInternados1 += Math.pow(Double.parseDouble(matriz5[2][2]) - (double) somaInternados1 / contadorNrDias, 2);
                        distMediaUCI1 += Math.pow(Double.parseDouble(matriz5[3][2]) - (double) somaUCI1 / contadorNrDias, 2);
                        distMediaObitos1 += Math.pow(Double.parseDouble(matriz5[4][2]) - (double) somaObitos1 / contadorNrDias, 2);
                    }

                    for (int linha = linhaInicial2Copia; linha < linhaInicial2Copia + contadorNrDias; linha++) {
                        String[][] matriz6 = criarMatrizDados(arr, arr[linha][0], linha - 1, linha, true);
                        distMediaInfetados2 += Math.pow(Double.parseDouble(matriz6[1][2]) - (double) somaInfetados2 / contadorNrDias, 2);
                        distMediaInternados2 += Math.pow(Double.parseDouble(matriz6[2][2]) - (double) somaInternados2 / contadorNrDias, 2);
                        distMediaUCI2 += Math.pow(Double.parseDouble(matriz6[3][2]) - (double) somaUCI2 / contadorNrDias, 2);
                        distMediaObitos2 += Math.pow(Double.parseDouble(matriz6[4][2]) - (double) somaObitos2 / contadorNrDias, 2);
                    }

                    String[][] matrizMediaDesvPadrao = new String[14][3];

                    matrizMediaDesvPadrao[0][1] = "  De  " + dataInicial1;
                    matrizMediaDesvPadrao[1][1] = "  At� " + converterFormatoData(arr[linhaInicial-1][0]);
                    matrizMediaDesvPadrao[0][2] = "De  " + dataInicial2;
                    matrizMediaDesvPadrao[1][2] = "At� " + converterFormatoData(arr[linhaInicial2-1][0]);
                    matrizMediaDesvPadrao[2][0] = "Varia��o Di�ria de Infetados";
                    matrizMediaDesvPadrao[3][0] = "      M�dia";
                    matrizMediaDesvPadrao[4][0] = "      Desvio Padr�o";
                    matrizMediaDesvPadrao[3][1] = String.valueOf((double) somaInfetados1 / contadorNrDias);
                    matrizMediaDesvPadrao[3][2] = String.valueOf((double) somaInfetados2 / contadorNrDias);
                    matrizMediaDesvPadrao[4][1] = String.valueOf(Math.sqrt(distMediaInfetados1 / contadorNrDias));
                    matrizMediaDesvPadrao[4][2] = String.valueOf(Math.sqrt(distMediaInfetados2 / contadorNrDias));
                    matrizMediaDesvPadrao[5][0] = "Varia��o Di�ria de Internados";
                    matrizMediaDesvPadrao[6][0] = matrizMediaDesvPadrao[3][0];
                    matrizMediaDesvPadrao[6][1] = String.valueOf((double) somaInternados1 / contadorNrDias);
                    matrizMediaDesvPadrao[6][2] = String.valueOf((double) somaInternados2 / contadorNrDias);
                    matrizMediaDesvPadrao[7][0] = matrizMediaDesvPadrao[4][0];
                    matrizMediaDesvPadrao[7][1] = String.valueOf(Math.sqrt(distMediaInternados1 / contadorNrDias));
                    matrizMediaDesvPadrao[7][2] = String.valueOf(Math.sqrt(distMediaInternados2 / contadorNrDias));
                    matrizMediaDesvPadrao[8][0] = "Varia��o Di�ria de Internados em UCI";
                    matrizMediaDesvPadrao[9][0] = matrizMediaDesvPadrao[3][0];
                    matrizMediaDesvPadrao[9][1] = String.valueOf((double) somaUCI1 / contadorNrDias);
                    matrizMediaDesvPadrao[9][2] = String.valueOf((double) somaUCI2 / contadorNrDias);
                    matrizMediaDesvPadrao[10][0] = matrizMediaDesvPadrao[4][0];
                    matrizMediaDesvPadrao[10][1] = String.valueOf(Math.sqrt(distMediaUCI1 / contadorNrDias));
                    matrizMediaDesvPadrao[10][2] = String.valueOf(Math.sqrt(distMediaUCI2 / contadorNrDias));
                    matrizMediaDesvPadrao[11][0] = "�bitos Di�rios";
                    matrizMediaDesvPadrao[12][0] = matrizMediaDesvPadrao[3][0];
                    matrizMediaDesvPadrao[12][1] = String.valueOf((double) somaObitos1 / contadorNrDias);
                    matrizMediaDesvPadrao[12][2] = String.valueOf((double) somaObitos2 / contadorNrDias);
                    matrizMediaDesvPadrao[13][0] = matrizMediaDesvPadrao[4][0];
                    matrizMediaDesvPadrao[13][1] = String.valueOf(Math.sqrt(distMediaObitos1 / contadorNrDias));
                    matrizMediaDesvPadrao[13][2] = String.valueOf(Math.sqrt(distMediaObitos2 / contadorNrDias));

                    if (interativo) {
                        imprimirMatrizMedias(matrizMediaDesvPadrao, System.out);

                        guardarFicheiro = guardarResultadoEmFicheiro();
                        if (!guardarFicheiro.equals("n")) {
                            File ficheiroOutput = new File("Outputs", guardarFicheiro);
                            PrintStream outTXT = new PrintStream(ficheiroOutput);
                            while (linhaInicialCopia <= linhaFinal && linhaInicial2Copia <= linhaFinal2) {
                                    String[][] matriz3 = criarMatrizDados(arr, arr[linhaInicialCopia][0], linhaInicialCopia - 1, linhaInicialCopia, true);
                                    linhaInicialCopia++;

                                    String[][] matriz4 = criarMatrizDados(arr, arr[linhaInicial2Copia][0], linhaInicial2Copia - 1, linhaInicial2Copia, true);
                                    linhaInicial2Copia++;

                                String[][] matrizComparativa = criarMatrizComparativa(matriz3, matriz4);
                                imprimirMatriz(matrizComparativa, outTXT);
                            }
                            imprimirMatrizMedias(matrizMediaDesvPadrao, outTXT);
                            System.out.println("Resultados guardados em " + ficheiroOutput.getAbsolutePath());
                            outTXT.close();
                        }
                    } else {
                        imprimirMatrizMedias(matrizMediaDesvPadrao, output);
                    }
            }
        }

        // M�todo que cria uma matriz para guardar os dados de duas matrizes diferentes, permitindo compar�-las

        public static String[][] criarMatrizComparativa(String[][] arr1, String[][] arr2) {
        String[][] matrizComparacao = new String[5][6];
        for (int linha = 0; linha < arr1.length; linha++) {
            for (int coluna = 0; coluna < matrizComparacao[0].length; coluna++) {
                if (coluna == 0){
                    matrizComparacao[linha][coluna] = arr1[linha][coluna];
                    if(arr1[linha][coluna]==null){
                        matrizComparacao[linha][coluna] = arr2[linha][coluna];
                    }
                }
                if (coluna < 3 && coluna !=0) {
                    matrizComparacao[linha][coluna] = arr1[linha][coluna];
                } else if (coluna == 3) {
                    matrizComparacao[linha][coluna] = arr2[linha][1];
                } else if (coluna == 4) {
                    matrizComparacao[linha][coluna] = arr2[linha][2];
                }
            }
        }

        for (int i = 1; i < matrizComparacao.length; i++) {
            if (matrizComparacao[i][2] != null && matrizComparacao[i][4] != null) {
                matrizComparacao[0][5] = "Diferen�as";
                matrizComparacao[i][5] = verificarSinal(Integer.parseInt(matrizComparacao[i][2]) - Integer.parseInt(matrizComparacao[i][4]));
            }
        }
        return matrizComparacao;
    }

    // M�todo para imprimir uma matriz com os dados formatados

    public static void imprimirMatriz(String[][] arr, PrintStream output) {
        for (int linha = 0; linha < arr.length; linha++) {
            for (int coluna = 0; coluna < arr[0].length; coluna++) {
                if (arr[linha][coluna] == null) {
                    output.printf("%20s", "");
                } else {
                    if (coluna == 0) {
                        output.printf("%-20s", arr[linha][coluna]);
                    } else {
                        output.printf("%20s", arr[linha][coluna]);
                    }
                }
            }
            output.println();
        }
        output.println();
    }

    // M�todo para imprimir a matriz das m�dias e desvios padr�o

    public static void imprimirMatrizMedias(String[][] arr, PrintStream output) {
        for (int linha = 0; linha < arr.length; linha++) {
            for (int coluna = 0; coluna < arr[0].length; coluna++) {
                if (arr[linha][coluna] == null) {
                    output.printf("%20s", "");
                } else {
                    if (coluna == 0) {
                        output.printf("%-20s", arr[linha][coluna]);
                    } else if ((coluna == 1 || coluna == 2) && linha > 2) {
                        output.printf("%20.4f", Double.parseDouble(arr[linha][coluna]));
                    } else {
                        output.printf("%20s", arr[linha][coluna]);
                    }
                }
            }
            output.println();
        }
    }

    // M�todo para verificar se o per�odo de tempo introduzido � v�lido

    public static boolean verificarPeriodoValido (String dataInicial, String dataFinal){
        LocalDate dateInicial = LocalDate.parse(converterFormatoData(dataInicial));
        LocalDate dateFinal = LocalDate.parse(converterFormatoData(dataFinal));
        if (dateFinal.isAfter(dateInicial)){
            return true;
        }
        return false;
    }

    // M�todo para descobrir a que dia da semana corresponde o dia da data introduzida

    public static int descobrirDiaSemana(String data) throws ParseException {
        Calendar cal = Calendar.getInstance();
        Date data1 = new SimpleDateFormat("yyyy-MM-dd").parse(data);
        cal.setTime(data1);
        return cal.get(Calendar.DAY_OF_WEEK);
    }
    // M�todo que retorna a linha do ficheiro em que est� presente a primeira "segunda-feira" do per�odo de tempo introduzido, para ser poss�vel determinar o in�cio da semana

    public static int verificarLinhaInicioSemana (String[][] arr, String dataInicial) throws ParseException {
        for (int linha = verificarLinhaDataFicheiro(arr,dataInicial); linha < arr.length; linha++) {
            if (descobrirDiaSemana(converterFormatoData(arr[linha][0])) == 2){
                return linha;
            }
        }
        return -1;
    }

    // M�todo que verifica e retorna a linha em que o m�s se inic�a

    public static int verificarLinhaInicioMes (String[][] arr, String data){
        String[] elements = (converterFormatoData(data)).split("-");
        if (Integer.parseInt(elements[2]) == 1){
            return verificarLinhaDataFicheiro(arr,elements[0] + "-" + elements[1] + "-" + elements[2]);
        }
        else {
            for (int linha = verificarLinhaDataFicheiro(arr,data); linha < arr.length; linha++) {
                elements = (converterFormatoData(arr[linha][0])).split("-");
                if (Integer.parseInt(elements[2]) == 1){
                    return verificarLinhaDataFicheiro(arr,elements[0] + "-" + elements[1] + "-" + elements[2]);
                }
            }
        }
        return -1;
    }

    // M�todo que verifica e retorna a linha em que o m�s termina

    public static int verificarLinhaFimMes (String[][] arr, String data){
        LocalDate lastDayOfMonth = LocalDate.parse(converterFormatoData(data), DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                .with(TemporalAdjusters.lastDayOfMonth());
        return verificarLinhaDataFicheiro(arr,String.valueOf(lastDayOfMonth));
    }

    // M�todo que retorna a soma do n�mero de �bitos num determinado per�odo de tempo

    public static int somarObitosPeriodoTempo (String[][] arr, String dataInicial, String dataFinal){
        int somaObitos = 0;
        for (int linha = verificarLinhaDataFicheiro(arr, converterFormatoData(dataInicial)); linha <= verificarLinhaDataFicheiro(arr, converterFormatoData(dataFinal)); linha++) {
            somaObitos += Integer.parseInt(arr[linha][5]);
        }
        return somaObitos;
    }

    // M�todo para criar a matriz de transi��o de Markov

    public static double[][] criarMatrizTransicaoMarkov(File input) throws FileNotFoundException {
        Scanner ler = new Scanner(input);
        int nrlinha, nrColuna;
        double[][] matrizTransicao = new double[5][5];
        while (ler.hasNextLine()) {
            String[] elements = ler.nextLine().split("=");
            if (elements.length > 1) {
                nrlinha = Integer.parseInt(String.valueOf(elements[0].charAt(1))) - 1;
                nrColuna = Integer.parseInt(String.valueOf(elements[0].charAt(2))) - 1;
                matrizTransicao[nrlinha][nrColuna] = Double.parseDouble(elements[1]);
            }
        }
        return matrizTransicao;
    }

    // Previs�es

    public static boolean verificarDataAntiga(String data, String primeiraData) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date data1 = sdf.parse(converterFormatoData(primeiraData));
        Date data2 = sdf.parse(converterFormatoData(data));
        if (data.split("-").length == 3){
            if (data1.compareTo(data2)<0){
                return true;
            }
            else{
                return false;
            }

        }
        else{
            return false;
        }
    }


    // M�todo que realiza a previs�o

    public static double[] previsaoMain(double[][] arr, String ultimaData, String[][] arrData){

        // Lista de informa��o referente ao �ltimo dia

        String[] listaInfo = new String[5];
        for (int i = 1; i < 6; i++) {
            listaInfo[i-1] = arrData[arrData.length-1][i];
        }


        // Alterar a matriz incial de Strings

        String[][] novaMatriz = new String[arrData.length-1][arrData[0].length-1];
        for (int i = 1; i < novaMatriz.length+1; i++) {
            for (int j = 0; j < novaMatriz[0].length; j++) {
                novaMatriz[i-1][j] = arrData[i][j+1];
            }
        }

        // Convers�o da matriz em inteiros

        int[][] novaMatrizInteiros = new int[novaMatriz.length][novaMatriz[0].length];
        novaMatrizInteiros = conversaoMatriz(novaMatriz);

        // Verificar se a data de previs�o est� contida no ficheiro

        int verificacao = -1, indice=-1;
        for (int i = 0; i < arrData.length; i++) {
            if (ultimaData.equals(converterFormatoData(arrData[i][0]))) {
                verificacao = 1;
                indice = i;
            }
        }


        if (verificacao == -1) {
            String primeiraData = arrData[arrData.length-1][0];
            double[] matriz = previsaoCalculo(arrData,primeiraData,ultimaData,arr,novaMatriz);
            return matriz;

        }
        else{
            String primeiraData = arrData[indice-1][0];
            double[] matriz = previsaoCalculo(arrData,primeiraData,ultimaData,arr,novaMatriz);
            return matriz;

        }
    }

    // M�todo que realiza o c�lculo da previs�o

    public static double[] previsaoCalculo(String [][] arrFicheiro,String diaInicial, String diaFinal, double[][] arr, String[][] matrizInteiros){        String[] listaDiaInicial = converterFormatoData(diaInicial).split("-");
        String[] listaDiaFinal = converterFormatoData(diaFinal).split("-");
        long numeroDias = numeroDias(listaDiaInicial,listaDiaFinal);
        double[] matriz = new double[matrizInteiros[0].length];
        for (int i = 0; i < matrizInteiros[0].length; i++) {
            matriz[i] = Double.parseDouble(matrizInteiros[verificarLinhaDataFicheiro(arrFicheiro,converterFormatoData(diaInicial))-1][i]);
        }

        for (int i = 0; i < numeroDias; i++) {
            matriz = multiplicacaoMatrizes(arr,matriz);
        }

        return matriz;
    }

    // M�todo que retorna o n�mero de dias entre a data dada pelo utilizador e a �ltima data do ficheiro

    public static long numeroDias(String[] data1, String[] data2){

        LocalDate dateBefore = LocalDate.of(Integer.parseInt(data1[0]), Month.of(Integer.parseInt(data1[1])), Integer.parseInt(data1[2]));
        LocalDate dateAfter = LocalDate.of(Integer.parseInt(data2[0]), Month.of(Integer.parseInt(data2[1])), Integer.parseInt(data2[2]));

        long numeroDias = ChronoUnit.DAYS.between(dateBefore,dateAfter);
        return numeroDias;
    }

    // M�todo que dadas duas matrizes retorna a matriz que representa a sua multiplica��o

    public static double[] multiplicacaoMatrizes(double[][] arr1, double[] arr2){
        double[] arrFinal = new double[arr2.length];
        int soma = 0, cont = 0,cont2=0;
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[0].length; j++) {
                soma +=  arr1[i][j] * arr2[cont];
                cont ++;
            }
            arrFinal[cont2] = soma;
            cont2++;
            soma =0;
            cont =0;
        }
        return arrFinal;
    }


    // M�todo que converte valores de uma matriz de Strings em Inteiros

    public static int[][] conversaoMatriz(String[][] matrizInicial){
        int[][] matrizFinal = new int[matrizInicial.length][matrizInicial[0].length];
        for (int i = 1; i < matrizInicial.length; i++) {
            for (int j = 0; j < matrizInicial[0].length; j++) {
                matrizFinal[i][j] = Integer.parseInt(matrizInicial[i][j]);
            }
        }
        return matrizFinal;
    }


    // M�todo que cria a matriz de previs�o

    public static String[][] criarMatrizPrevisao (double[] previsao, String data){
        String [][] matrizPrevisao = new String[6][2];
        matrizPrevisao[0][0] = "Previs�o de dados para a data "+data;
        matrizPrevisao[1][0] = "N�o Infetados";
        matrizPrevisao[2][0] = "Casos Ativos";
        matrizPrevisao[3][0] = "Internamentos";
        matrizPrevisao[4][0] = "Internamentos em UCI";
        matrizPrevisao[5][0] = "�bitos";

        for (int linha = 1; linha <= previsao.length; linha++) {
            matrizPrevisao[linha][1] = String.valueOf(previsao[linha-1]);
        }
        return matrizPrevisao;
    }

    // M�todo para imprimir a matriz de previs�o

    public static void imprimirMatrizPrevisao (String [][] arr,FileOutputStream outputFile, boolean interativo) throws FileNotFoundException {
        PrintStream output = new PrintStream(outputFile);
        String guardarFicheiro;
        if (interativo) {
            output = new PrintStream(System.out);
        }

        imprimirMatriz(arr,output);

        if(interativo){
            guardarFicheiro = guardarResultadoEmFicheiro();
            if (!guardarFicheiro.equals("n")){
                File ficheiroOutput = new File("Outputs", guardarFicheiro);
                PrintStream outTXT = new PrintStream(ficheiroOutput);
                imprimirMatriz(arr,outTXT);
                System.out.println("Resultados guardados em " + ficheiroOutput.getAbsolutePath());
                outTXT.close();
            }

        }

    }

    // Decomposi��o de Lu e M�todo de Crout

    public static double[][] matriz1I(int lenght,int lenght0) {
        double[][]I=new double[lenght][lenght0];
        for (int i = 0; i < lenght; i++) {
            for (int j = 0; j < lenght0; j++) {
                I[i][j]=1;
            }
        }
        return I;
    }

    public static double[][] matrizIdentidade(int length,int length2) {
        double [][]Identidade=new double[length][length2];
        for (int i = 0; i <length ; i++) {
            for (int j = 0; j < length2; j++) {
                Identidade[i][j]=0;
                if (i==j){
                    Identidade[i][j]=1;
                }
            }
        }
        return Identidade;
    }

    public static double[][] matrizInversaU(double[][]matrizU) {
        double [][]matrizC=new double[matrizU.length][matrizU.length];
        for (int i = 0; i <matrizU.length ; i++) {
            for (int j = 0; j < matrizU.length; j++) {
                matrizC[i][j]=0;
            }

        }

        //1� diagonal da matrizU inversa
        for (int i = 0; i < matrizU.length; i++) {
            matrizC[i][i]=1/matrizU[i][i];
        }
        //2� diagonal da matrizU inversa
        for (int i = 1; i < matrizU.length; i++) {
            matrizC[i-1][i]=-(matrizU[i-1][i]*matrizC[i][i])/matrizU[i-1][i-1];
        }
        //3� diagonal da matrizU inversa
        for (int i = 2; i < matrizU.length; i++) {
            matrizC[i-2][i]=-(matrizC[i-1][i]*matrizU[i-2][i-1]+matrizC[i][i]*matrizU[i-2][i])/matrizU[i-2][i-2];
        }
        //4� diagonal
        matrizC[0][3]=-(matrizC[1][3]*matrizU[0][1]+matrizC[2][3]*matrizU[0][2]+matrizC[3][3]*matrizU[0][3])/matrizU[0][0];
        return matrizC;
    }

    public static double[][] matrizInversaL (double[][] matrizL) {
        double[][] matrizC;
        matrizC=matrizIdentidade(4,4);
        //1� diagonal da matrizL inversa
        for (int i = 1; i < matrizL.length; i++) {
            matrizC[i][i-1]=-matrizL[i][i-1];
        }
        //2� diagonal da matrizL inversa
        for (int i = 2; i < matrizL.length; i++) {
            matrizC[i][i-2]=-(matrizL[i][i-2]+matrizC[i-1][i-2]*matrizL[i][i-1]);
        }
        //3� diagonal da matrizL inversa
        matrizC[3][0]=-(matrizL[3][0]+matrizC[1][0]*matrizL[3][1]+matrizC[2][0]*matrizL[3][2]);

        return matrizC;
    }

    public static double[][] matrizL_matrizU (double[][]matrizA,String matriz) {

        double[][]matrizL=new double[matrizA.length][matrizA[0].length];
        double[][]matrizU=new double[matrizA.length][matrizA[0].length];
        for (int i = 0; i < matrizL.length; i++) {
            for (int j = 0; j < matrizL[0].length; j++) {
                matrizL[i][j]=0;
                matrizU[i][j]=0;
                matrizL[i][j]=matrizIdentidade(4,4)[i][j];
            }
        }
        //linha 0 da matrizU
        for (int i = 0; i < matrizA.length; i++) {
            matrizU[0][i]=matrizA[0][i];
        }
        //coluna 0 da matrizL
        for (int i = 1; i < 4; i++) {
            matrizL[i][0]=matrizA[i][0]/matrizU[0][0];
        }
        //linha 1 da matrizU
        for (int i = 1; i < matrizA.length; i++) {
            matrizU[1][i]=(matrizA[1][i]-(matrizU[0][i]*matrizL[1][0]));

        }
        //coluna 1 da matrizL
        for (int i = 2; i < matrizL.length; i++) {
            matrizL[i][1]=(matrizA[i][1]-(matrizU[0][1]*matrizL[i][0]))/matrizU[1][1];
        }
        //linha 2 da matrizU
        for (int i = 2; i < matrizA.length; i++) {
            matrizU[2][i]=(matrizA[2][i]-(matrizU[0][i]*matrizL[2][0]+matrizU[1][i]*matrizL[2][1]));
        }
        //coluna 2 da matrizL
        matrizL[3][2]=(matrizA[3][2]-(matrizU[0][3]*matrizL[3][0]+matrizU[1][2]*matrizL[3][1]))/matrizU[2][2];

        //linha 3 da matrizU
        matrizU[3][3]=(matrizA[3][3]-(matrizU[0][3]*matrizL[3][0]+matrizU[1][3]*matrizL[3][1]+matrizU[2][3]*matrizL[3][2]));


        if (matriz.equals("l")  /*%% matriz.equals("l") %% matriz.equals("matrizL")%%matriz.equals("matrizl")*/){
            return matrizL;
        }else{
            return matrizU;
        }
    }

    public static double[][] matrizQ (double [][]matrizA) {
        double [][]matriz1Q=new double[matrizA.length-1][matrizA.length-1];
        for (int i = 0; i < matriz1Q.length; i++) {
            for (int j = 0; j < matriz1Q.length; j++) {
                matriz1Q[i][j]=matrizA[i][j];
            }
        }
        return matriz1Q;
    }

    public static double multiplicacaoMatrizes_parte(double[][] firstMatrix, double[][] secondMatrix, int row, int col) {
        double cell = 0;
        for (int i = 0; i < secondMatrix.length; i++) {
            cell += firstMatrix[row][i] * secondMatrix[i][col];
        }
        return cell;
    }

    public static double[][] multiplicacaoMatrizes(double[][] matriz1, double[][] matriz2) {
        double[][] result = new double[matriz1.length][matriz2[0].length];

        for (int linha = 0; linha < result.length; linha++) {
            for (int coluna = 0; coluna < result[linha].length; coluna++) {
                result[linha][coluna] = multiplicacaoMatrizes_parte(matriz1, matriz2, linha, coluna);
            }
        }

        return result;
    }

    public static double[][] matrizM(double[][] q) {
        int x=4;
        double[][]matrizQ=matrizQ(q);
        matrizQ= subtracaoMatrizes(matrizIdentidade(x,x),matrizQ);

        double [][]matrizU_inversa=matrizInversaU(matrizL_matrizU(matrizQ,"u"));
        double[][]matrizL_inversa=matrizInversaL(matrizL_matrizU(matrizQ,"l"));

        double[][]Q_inversa;
        Q_inversa=multiplicacaoMatrizes(matrizU_inversa,matrizL_inversa);

        //matriz M
        Q_inversa=multiplicacaoMatrizes(matriz1I(1,4),Q_inversa);
        return Q_inversa;
    }

    public static double[][] subtracaoMatrizes(double[][] matrizIdentidade, double[][] matrizQ ){
        double[][] matrizFinal = new double[matrizIdentidade.length][matrizIdentidade[0].length];
        for (int i = 0; i < matrizIdentidade.length; i++) {
            for (int j = 0; j < matrizIdentidade[0].length; j++) {
                matrizFinal[i][j] = matrizIdentidade[i][j] - matrizQ[i][j];
            }
        }
        return matrizFinal;
    }

    // M�todo para criar uma matriz com a previs�o de dias at� ao �bito

    public static String[][] criarMatrizPrevisaoDiasAteObito (double[][] previsaoDias){
        String [][] matrizPrevisaoDiasAteObito = new String[5][2];
        matrizPrevisaoDiasAteObito[0][0] = "N�mero esperado de dias at� � morte:";
        matrizPrevisaoDiasAteObito[1][0] = "    N�o Infetado";
        matrizPrevisaoDiasAteObito[2][0] = "    Infetado";
        matrizPrevisaoDiasAteObito[3][0] = "    Hospitalizado";
        matrizPrevisaoDiasAteObito[4][0] = "    Hospitalizado em UCI";

        for (int linha = 0; linha < previsaoDias[0].length; linha++) {
            matrizPrevisaoDiasAteObito[linha+1][1] = String.valueOf(previsaoDias[0][linha]);
        }
        return matrizPrevisaoDiasAteObito;
    }

    // M�todo para imprimir uma matriz com a previs�o de dias at� ao �bito

    public static void imprimirMatrizPrevisaoDiasAteObito (String [][] arr,FileOutputStream outputFile, boolean interativo) throws FileNotFoundException {
        PrintStream output = new PrintStream(outputFile);
        String guardarFicheiro;
        if (interativo) {
            output = new PrintStream(System.out);
        }

        for (int linha = 0; linha < arr.length; linha++) {
            for (int coluna = 0; coluna < arr[0].length; coluna++) {
                if (arr[linha][coluna] == null) {
                    output.printf("%24s", "");
                } else {
                    if (coluna == 0) {
                        output.printf("%-24s", arr[linha][coluna]);
                    } else {
                        output.printf("%24.1f", Double.parseDouble(arr[linha][coluna]));
                    }
                }
            }
            output.println();
        }

        if(interativo){
            guardarFicheiro = guardarResultadoEmFicheiro();
            if (!guardarFicheiro.equals("n")){
                File ficheiroOutput = new File("Outputs", guardarFicheiro);
                PrintStream outTXT = new PrintStream(ficheiroOutput);
                for (int linha = 0; linha < arr.length; linha++) {
                    for (int coluna = 0; coluna < arr[0].length; coluna++) {
                        if (arr[linha][coluna] == null) {
                            outTXT.printf("%24s", "");
                        } else {
                            if (coluna == 0) {
                                outTXT.printf("%-24s", arr[linha][coluna]);
                            } else {
                                outTXT.printf("%24.1f", Double.parseDouble(arr[linha][coluna]));
                            }
                        }
                    }
                    outTXT.println();
                }
                System.out.println("Resultados guardados em " + ficheiroOutput.getAbsolutePath());
                outTXT.close();
            }

        }

    }

    // Testes unit�rios

    public static void correrTestes() throws FileNotFoundException, ParseException {
        File lerFicheiro = new File("Inputs/totalPorEstadoCovid19EmCadaDia.csv");
        File matrizTransicao = new File("Inputs/exemploMatrizTransicoes.txt");

        String [][] matrizCovid = criarMatrizCovid(lerFicheiro);
        String [][] matrizDados = criarMatrizDados(matrizCovid,"",2,3,true);
        System.out.println("countLinhasMatrizCovid" + (testCountLinhasMatrizCovid(countLinhasMatrizCovid(lerFicheiro), 434) ? " OK" : " NOT OK" + "\n"));
        System.out.println("countColunasMatrizCovid" + (testCountColunasMatrizCovid(countColunasMatrizCovid(lerFicheiro), 6) ? " OK" : " NOT OK" + "\n"));
        System.out.println("criarMatrizCovid" + (testCriarMatrizCovid(criarMatrizCovid(lerFicheiro),434,6) ? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarSeDadosAcumulados" + (testVerificarSeDadosAcumulados(verificarSeDadosAcumulados(criarMatrizCovid(lerFicheiro)), false) ? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarSinal" + (testVerificarSinal(verificarSinal(4), "+4") ? " OK" : " NOT OK" + "\n"));
        System.out.println("converterFormatoData" + (testConverterFormatoData(converterFormatoData("21-1-2021"),"2021-01-21") ? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarDataValida" + (testVerificarDataValida(verificarDataValida("2020-02-31"), false) ? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarAnoBissexto" + (testVerificarAnoBissexto(verificarAnoBissexto(2017), false) ? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarLinhaDataFicheiro" + (testVerificarLinhaDataFicheiro(verificarLinhaDataFicheiro(criarMatrizCovid(lerFicheiro),"2021-11-27"), 392) ? " OK" : " NOT OK" + "\n"));
        System.out.println("criarMatrizDados" + (testCriarMatrizDados(matrizDados,5,3) ? " OK" : " NOT OK" + "\n"));
        System.out.println("criarMatrizComparativa" + (testCriarMatrizComparativa(criarMatrizComparativa(matrizDados,matrizDados),5,6) ? " OK" : " NOT OK" + "\n"));
        System.out.println("descobrirDiaSemana" + (testdescobrirDiaSemana(descobrirDiaSemana("2022-1-22"),7) ? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarLinhaInicioSemana" + (testVerificarLinhaInicioSemana(verificarLinhaInicioSemana(matrizCovid,"2021-12-22"),422) ? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarLinhaInicioMes" + (testVerificarLinhaInicioMes(verificarLinhaInicioMes(matrizCovid,"2021-11-30"),396) ? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarLinhaFimMes" + (testVerificarLinhaFimMes(verificarLinhaFimMes(matrizCovid,"2021-12-25"),426) ? " OK" : " NOT OK" + "\n"));
        System.out.println("somarObitosPeriodoTempo" + (testSomarObitosPeriodoTempo(somarObitosPeriodoTempo(matrizCovid,"01-11-2020","06-11-2020"),285) ? " OK" : " NOT OK" + "\n"));
        System.out.println("criarMatrizTransicaoMarkov" + (testCriarMatrizTransicaoMarkov(criarMatrizTransicaoMarkov(matrizTransicao),5,5) ? " OK\n" : " NOT OK" + "\n"));
        System.out.println("tipoFicheiro" + (testTipoFicheiro(tipoFicheiro(lerFicheiro),1) ? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarMultiplicacaoMatrizes" + ((verificarMultiplicacaoMatrizes(matrizTeste2(10.0,5),multiplicacaoMatrizes(matrizTeste1(1.0,5,5),matrizTeste2(2.0,5))))? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarConversaoMatrizes" + ((verificarConversaoMatriz(conversaoMatriz(matrizTeste3("1",2,2))))? " OK" : " NOT OK" + "\n"));
        System.out.println("verificarNumeroDias" + ((verificarNumeroDias("2020-01-05","2020-01-10", 5)? " OK" : " NOT OK" + "\n")));

        //M�todos que exigem input por parte do Utilizador
        System.out.println("guardarResultadoEmFicheiro" + (testGuardarResultadoEmFicheiro(guardarResultadoEmFicheiro(),"ficheiro.csv") ? " OK" : " NOT OK" + "\n"));
        System.out.println();
        System.out.println("carregarFicheiro" + (testCarregarFicheiro(carregarFicheiro(),lerFicheiro) ? " OK" : " NOT OK" + "\n"));
        System.out.println();
    }

    public static boolean testCountLinhasMatrizCovid(int nrLinhas, int nrLinhasExpectavel){
        if(nrLinhas == nrLinhasExpectavel){
            return true;
        }
        return false;
    }

    public static boolean testCountColunasMatrizCovid (int nrColunas, int nrColunasExpectavel){
        if(nrColunas == nrColunasExpectavel){
            return true;
        }
        return false;
    }

    public static boolean testCriarMatrizCovid (String [][] matrizCovid, int nrLinhas, int nrColunas){
        if(matrizCovid.length == nrLinhas && matrizCovid[0].length == nrColunas){
            return true;
        }
        return false;
    }

    public static boolean testVerificarSeDadosAcumulados(boolean acumulado, boolean teste) {
        if (acumulado == teste) {
            return true;
        }
        return false;
    }

    public static boolean testVerificarSinal(String sinal, String teste) {
        if (sinal.equals(teste)) {
            return true;
        }
        return false;
    }

    public static boolean testConverterFormatoData (String data, String dataExpectavel){
        if (data.equals(dataExpectavel)) {
            return true;
        }
        return false;
    }

    public static boolean testVerificarDataValida (boolean dataValida, boolean verificarData){
        if(dataValida == verificarData){
            return true;
        }
        return false;
    }

    public static boolean testVerificarAnoBissexto (boolean ano, boolean bissexto){
        if(ano == bissexto){
            return true;
        }
        return false;
    }

    public static boolean testVerificarLinhaDataFicheiro (int linha, int linhaExpectavel) {
        if (linha == linhaExpectavel) {
            return true;
        }
        return false;
    }

    public static boolean testGuardarResultadoEmFicheiro (String resultado, String resultadoExpectavel) {
        if (resultado.equals(resultadoExpectavel)) {
            return true;
        }
        return false;
    }

    public static boolean testCriarMatrizDados (String [][] matrizDados, int nrLinhas, int nrColunas){
        if(matrizDados.length == nrLinhas && matrizDados[0].length == nrColunas){
            return true;
        }
        return false;
    }

    public static boolean testTipoFicheiro (int tipoFicheiro, int tipoFicheiroExpectavel){
        if(tipoFicheiro == tipoFicheiroExpectavel){
            return true;
        }
        return false;
    }


    public static boolean testCarregarFicheiro (File ficheiroIntroduzido, File ficheiroExpectavel){
        if(ficheiroIntroduzido.getAbsolutePath().equals(ficheiroExpectavel.getAbsolutePath())){
            return true;
        }
        return false;
    }

    public static boolean testCriarMatrizComparativa (String [][] matrizComparativa, int nrLinhas, int nrColunas){
        if(matrizComparativa.length == nrLinhas && matrizComparativa[0].length == nrColunas){
            return true;
        }
        return false;
    }

    public static boolean testdescobrirDiaSemana (int diaSemana, int diaSemanaExpectavel){
        if(diaSemana == diaSemanaExpectavel){
            return true;
        }
        return false;
    }

    public static boolean testVerificarLinhaInicioSemana (int LinhaInicioSemana, int LinhaInicioSemanaExpectavel){
        if(LinhaInicioSemana == LinhaInicioSemanaExpectavel){
            return true;
        }
        return false;
    }

    public static boolean testVerificarLinhaInicioMes (int LinhaInicioMes, int LinhaInicioMesExpectavel){
        if(LinhaInicioMes == LinhaInicioMesExpectavel){
            return true;
        }
        return false;
    }

    public static boolean testVerificarLinhaFimMes (int LinhaFimMes, int LinhaFimMesExpectavel){
        if(LinhaFimMes == LinhaFimMesExpectavel){
            return true;
        }
        return false;
    }

    public static boolean testSomarObitosPeriodoTempo(int somaObitos, int somaObitosExpectavel){
        if(somaObitos == somaObitosExpectavel){
            return true;
        }
        return false;
    }

    public static boolean testCriarMatrizTransicaoMarkov (double [][] matrizTransicao, int nrLinhas, int nrColunas){
        if(matrizTransicao.length == nrLinhas && matrizTransicao[0].length == nrColunas){
            return true;
        }
        return false;
    }

    public static boolean verificarMultiplicacaoMatrizes(double[] matrizResul, double[] matrizResul2){
        int contador =0;
        for (int i = 0; i < matrizResul.length; i++) {
            if (matrizResul[i] == matrizResul2[i]){
                contador ++;
            }
            if (contador==5){
                return true;
            }
        }
        return false;
    }

    public static boolean verificarConversaoMatriz(int[][] matrizInicialInt){
        int contador =0;
        for (int i = 0; i < matrizInicialInt.length; i++) {
            for (int j = 0; j < matrizInicialInt[0].length; j++) {
                if (matrizInicialInt[i][j] == 1){
                    contador ++;
                }

            }
        }
        if (contador == 2){
            return true;
        }
        else{
            return false;
        }

    }

    public static boolean verificarNumeroDias(String dataInicial, String dataFinal, int NumeroDias){
        if (NumeroDias== numeroDias(dataInicial.split("-"),dataFinal.split("-"))){
            return true;
        }
        return false;
    }


    public static double[][] matrizTeste1(double Valor, int colunas, int linhas){
        double[][] matriz = new double[linhas][colunas];
        for (int i = 0; i < linhas; i++) {
            for (int j = 0; j < colunas; j++) {
                matriz[i][j] = Valor;
            }
        }
        return matriz;
    }

    public static double[] matrizTeste2(double Valor, int comprimento){
        double[] matriz = new double[comprimento];
        for (int i = 0; i < comprimento; i++) {
            matriz[i] = Valor;
        }
        return matriz;
    }

    public static String[][] matrizTeste3(String Valor, int linhas, int colunas){
        String[][] matriz = new String[linhas][colunas];
        for (int i = 0; i < linhas; i++) {
            for (int j = 0; j < colunas; j++) {
                matriz[i][j] = Valor;
            }
        }
        return matriz;
    }

}